import React, {useContext, useEffect, useState, useRef} from 'react';
import {View, Text} from 'react-native';
import styles from '../styles';
import {THEME} from '../components/theme';
import {TodoContext} from '../context/todo/todoContext';
import voice from '../services/VoiceService';
import {DEFAULT_TEXT, MAX_AUDIO_LENGTH} from '../constants';
import Pulse from '../components/ui/PulceCircle';

const useInterval = (callback, delay) => {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    const tick = () => savedCallback.current();

    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
};

export const RecordScreen = ({navigation}) => {
  const {addTodo} = useContext(TodoContext);

  const [memo, setMemo] = useState(DEFAULT_TEXT);
  const [airOn, setAir] = useState(false);

  const [count, setCount] = useState(0);

  useInterval(
    () => {
      if (count === MAX_AUDIO_LENGTH - 1) {
        toggleRecording();
      }
      setCount(count + 1);
    },
    airOn ? 1000 : null,
  );

  const toggleRecording = useCallback(async () => {
    if (!airOn) {
      await voice.startRecording(note => setMemo(note));
    } else {
      voice.stopRecognizing(() => {
        addTodo(memo);
        setCount(0);
        setMemo(DEFAULT_TEXT);
        navigation.navigate('Main');
      });
    }
    setAir(() => !airOn);
  });

  useEffect(() => {
    navigation.addListener('didFocus', () => {
      if (!airOn) {
        toggleRecording();
      }
    });
    toggleRecording();
  }, [airOn, navigation, toggleRecording]);

  return (
    <View style={styles.sectionContainer}>
      <View
        style={{
          ...styles.sectionContainer,
          ...{textAlign: 'center'},
        }}>
        <Text style={styles.sectionDescription}> {memo} </Text>
      </View>
      <View
        style={{
          ...styles.sectionContainer,
          ...{alignItems: 'center'},
        }}>
        {count !== 0 && (
          <Pulse
            color={THEME.MAIN_COLOR}
            numPulses={6}
            diameter={300}
            initialDiameter={100}
            speed={10}
            duration={500}
            text={`${MAX_AUDIO_LENGTH - count} sec`}
          />
        )}
      </View>
    </View>
  );
};
