import React, { useState, useContext } from 'react'
import { StyleSheet, View, Text, Button } from 'react-native'
import { THEME } from '../components/theme'
import { AppCard } from '../components/ui/AppCard'
import { EditModal } from '../components/EditModal'
import {TodoContext} from '../context/todo/todoContext';

export const TodoScreen = ({ navigation }) => {
  const { todos, updateTodo } = useContext(TodoContext);
  const [ modal, setModal ] = useState(false);

  const todo = todos.find(t => t.id === navigation.getParam('todoId'));

  const saveHandler = title => {
    updateTodo(todo.id, title);
    setModal(false)
  };

  return (
    <View>
      <EditModal
        value={todo.title}
        visible={modal}
        onCancel={() => setModal(false)}
        onSave={saveHandler}
      />

      <AppCard style={styles.card}>
        <Text style={styles.title}>{todo.title}</Text>
        <Button title='Edit' onPress={() => setModal(true)} />
      </AppCard>
    </View>
  )
};

TodoScreen.navigationOptions = ({ navigation}) => {
  const id = navigation.getParam('todoId');

  return {
    headerTitle: `Todo : ${id}`,
  }
};

const styles = StyleSheet.create({
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  card: {
    marginBottom: 20,
    padding: 15
  },
  button: {
    width: '40%'
  },
  title: {
    fontSize: 20
  }
});
