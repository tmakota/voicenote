import React, {useEffect} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import styles from '../styles';
import {StartPicker, EndPicker, IntervalPicker, LangPicker} from './Pickers';
import {useSettings, usePicker} from '../hooks';

export const SettingsScreen = () => {
  const [
    {intervalPicker, endPicker, startPicker, langPicker},
    setPicker,
  ] = usePicker();

  const [{start, end, interval, language}, setProperty] = useSettings();

  const closePicker = async (name, value) => {
    setProperty(name, value);
    setPicker(name, false);
  };

  useEffect(() => {
    setProperty();
  }, []);

  return (
    <View style={styles.sectionContainer}>
      <View style={styles.sectionContainer}>
        {!intervalPicker && (
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => setPicker('interval', true)}
            style={styles.icons}>
            <View style={styles.settingsRow}>
              <View style={styles.settingsRowColumn}>
                <Text>Interval of reminders</Text>
              </View>
              <View style={styles.settingsRowColumn}>
                <Text>{interval / 60} hour</Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
        {intervalPicker && (
          <IntervalPicker
            interval={interval}
            selectInterval={value => closePicker('interval', value)}
          />
        )}
      </View>
      <View style={styles.sectionContainer}>
        {!startPicker && (
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => setPicker('start', true)}
            style={styles.icons}>
            <View style={styles.settingsRow}>
              <View style={styles.settingsRowColumn}>
                <Text>Start time reminders</Text>
              </View>
              <View style={styles.settingsRowColumn}>
                <Text>{start}H:00min</Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
        {startPicker && (
          <StartPicker
            start={start}
            selectStart={value => closePicker('start', value)}
          />
        )}
      </View>
      <View style={styles.sectionContainer}>
        {!endPicker && (
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => setPicker('end', true)}
            style={styles.icons}>
            <View style={styles.settingsRow}>
              <View style={styles.settingsRowColumn}>
                <Text>End time reminders</Text>
              </View>
              <View style={styles.settingsRowColumn}>
                <Text>{end}H:00min</Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
        {endPicker && (
          <EndPicker end={end} selectEnd={value => closePicker('end', value)} />
        )}
      </View>
      <View style={styles.sectionContainer}>
        {!langPicker && (
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => setPicker('language', true)}
            style={styles.icons}>
            <View style={styles.settingsRow}>
              <View style={styles.settingsRowColumn}>
                <Text>Current language</Text>
              </View>
              <View style={styles.settingsRowColumn}>
                <Text>{language}</Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
        {langPicker && (
          <LangPicker
            language={language}
            selectLanguage={value => closePicker('language', value)}
          />
        )}
      </View>
    </View>
  );
};
