import React, {useContext, useEffect, useState} from 'react';
import {View, Text, Button} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import styles from '../styles';
import {TodoContext} from '../context/todo/todoContext';
import {DEFAULT_TEXT} from '../constants';
import {RecordScreen} from './RecordScreen';

export const NewNoteScreen = ({navigation}) => {
  const {addTodo} = useContext(TodoContext);

  const [isOnline, setOnline] = useState(false);

  const completeJob = () => {
    addTodo(DEFAULT_TEXT);
    navigation.navigate('Main');
  };

  useEffect(() => {
    NetInfo.fetch().then(state => setOnline(state.isConnected));
    NetInfo.addEventListener('connectionChange', state => setOnline(state.isConnected));
  }, []);

  return (
    <View style={styles.sectionContainer}>
      {isOnline ? (
        <RecordScreen navigation={navigation}/>
      ) : (
        <View
          style={{
            ...styles.sectionContainer,
            ...styles.warningContainer,
          }}>
          <Text style={styles.warning}>{`You are offline, voice recognition is not available in this case. 
        You can create template note and edit it.`}</Text>
          <Button style={styles.button} title="Create empty note" onPress={completeJob} />
          <Button style={styles.button} title="Back" onPress={() => navigation.navigate('Main')} />
        </View>
      )}
    </View>
  );
};
