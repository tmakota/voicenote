import { Picker } from 'react-native';
import React from 'react';
import styles from '../styles';
import { LOCALES } from '../constants';

const EndPicker = ({ end, selectEnd }) => <Picker
  selectedValue={end}
  style={styles.pickerStyles}
  prompt={'select end time'}
  onValueChange={selectEnd}>
  <Picker.Item label="1 PM" value="13" />
  <Picker.Item label="2 PM" value="14" />
  <Picker.Item label="3 PM" value="15" />
  <Picker.Item label="4 PM" value="16" />
  <Picker.Item label="5 PM" value="17" />
  <Picker.Item label="6 PM" value="18" />
  <Picker.Item label="7 PM" value="19" />
  <Picker.Item label="8 PM" value="20" />
  <Picker.Item label="9 PM" value="21" />
  <Picker.Item label="10 PM" value="22" />
  <Picker.Item label="11 PM" value="23" />
  <Picker.Item label="12 PM" value="24" />
</Picker>;

const StartPicker = ({ start, selectStart }) => <Picker
  selectedValue={start}
  style={styles.pickerStyles}
  prompt={'select start time'}
  onValueChange={selectStart}>
  <Picker.Item label="1 AM" value="1" />
  <Picker.Item label="2 AM" value="2" />
  <Picker.Item label="3 AM" value="3" />
  <Picker.Item label="4 AM" value="4" />
  <Picker.Item label="5 AM" value="5" />
  <Picker.Item label="6 AM" value="6" />
  <Picker.Item label="7 AM" value="7" />
  <Picker.Item label="8 AM" value="8" />
  <Picker.Item label="9 AM" value="9" />
  <Picker.Item label="10 AM" value="10" />
  <Picker.Item label="11 AM" value="11" />
  <Picker.Item label="12 AM" value="12" />
</Picker>;

const IntervalPicker = ({ interval, selectInterval }) => <Picker
  selectedValue={interval}
  style={styles.pickerStyles}
  prompt={'select interval time'}
  onValueChange={selectInterval}>
  <Picker.Item label="1h" value="60" />
  <Picker.Item label="2h" value="120" />
  <Picker.Item label="3h" value="180" />
  <Picker.Item label="4h" value="240" />
</Picker>;

const LangPicker = ({ language, selectLanguage }) => <Picker
  selectedValue={language}
  style={styles.pickerStyles}
  prompt={'select language'}
  onValueChange={selectLanguage}>
  {
    LOCALES.map(({ lang, code }) => <Picker.Item label={lang} value={code} key={code} />)
  }
</Picker>;

export { StartPicker, EndPicker, IntervalPicker, LangPicker }
