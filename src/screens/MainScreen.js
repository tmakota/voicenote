import React, {useContext, useState, useEffect} from 'react';
import {View, FlatList, Text, Alert, TouchableOpacity} from 'react-native';
import {Todo} from '../components/Todo';
import {EditModal} from '../components/EditModal';

import {TodoContext} from '../context/todo/todoContext';
import styles from '../styles';
import shedule from '../services/ShedulerService';
import storage from '../services/StorageService';

export const MainScreen = () => {
  const {todos, removeTodo, setTodos, updateTodo} = useContext(TodoContext);

  const [todo, setTodo] = useState(null);

  const [modal, setModal] = useState(false);

  const cancelHandler = () => {
    setModal(false);
    setTodo(null);
  };

  const saveHandler = title => {
    if (title.trim().length < 3) {
      Alert.alert(
        'Error!',
        `Must be at least 3 symbols. Now ${title.trim().length} only.`,
      );
    } else {
      updateTodo(todo.id, title);
      cancelHandler();
    }
  };

  const onOpen = selected => {
    setTodo(selected);
    setModal(true);
  };

  useEffect(() => {
    // storage.resetApp()
    shedule.setList(list => setTodos(list));
  }, []);

  return (
    <View style={styles.sectionContainer}>
      {todo && (
        <EditModal
          value={todo.title}
          visible={modal}
          onCancel={cancelHandler}
          onSave={saveHandler}
        />
      )}
      <View style={styles.headerMain}>
        <Text style={styles.header}>Hi there!</Text>
      </View>
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => {
          setTodos([])
          storage.resetApp()
        }}
        style={styles.icons}>
        <Text>Clear notes</Text>
      </TouchableOpacity>
      <View style={{...styles.sectionContainer, ...{flex: 3}}}>
        {todos.length > 0 ? (
          <FlatList
            keyExtractor={item => item.id.toString()}
            data={todos}
            renderItem={({item}) => (
              <Todo todo={item} onRemove={removeTodo} onOpen={onOpen} />
            )}
          />
        ) : (
          <View style={styles.sectionContainer}>
            <Text style={styles.sectionTitle}>No todos yet</Text>
          </View>
        )}
      </View>
    </View>
  );
};
