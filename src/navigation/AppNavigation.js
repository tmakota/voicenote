import React from 'react';
import {View, Platform, Image} from 'react-native';
import {createAppContainer, withNavigation} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {MainScreen} from '../screens/MainScreen';
import {TodoScreen} from '../screens/TodoScreen';
import {THEME} from '../components/theme';
import {SettingsScreen} from '../screens/SettingsScreen';
import {NewNoteScreen} from '../screens/NewNoteScreen';
import styles from '../styles';

const defaultNavigationOptions = {
  headerStyle: {
    backgroundColor: THEME.BG_COLOR,
  },
  headerTintColor: THEME.ICONS_COLOR,
};

const TodoNavigator = createStackNavigator(
  {
    Main: MainScreen,
    Todo: {
      screen: TodoScreen,
    },
  },
  {
    initialRouteName: 'Main',
    headerMode: 'none',
  },
);

const SettingsNavigator = createStackNavigator(
  {
    Settings: SettingsScreen,
  },
  {
    defaultNavigationOptions,
  },
);

const RecorderNavigator = createStackNavigator(
  {
    Record: NewNoteScreen,
  },
  {
    defaultNavigationOptions,
    headerMode: 'none',
  },
);

const FooterNavigation = createBottomTabNavigator(
  {
    List: {
      screen: TodoNavigator,
      navigationOptions: {
        title: 'List',
        tabBarIcon: () => (
          <View style={styles.tab}>
            <Image source={require('../assets/icons/list/Active.png')} />
          </View>
        ),
      },
    },
    Record: {
      screen: withNavigation(RecorderNavigator),
      navigationOptions: {
        tabBarVisible: false,
        title: 'Record',
        tabBarIcon: () => (
          <View style={styles.recorder}>
            <Image source={require('../assets/icons/add/Hover.png')} />
          </View>
        ),
      },
    },
    Settings: {
      screen: SettingsNavigator,
      navigationOptions: {
        title: 'Settings',
        tabBarIcon: () => (
          <View style={styles.tab}>
            <Image source={require('../assets/icons/settings/Active.png')} />
          </View>
        ),
      },
    },
  },
  {
    initialRouteName: 'List',
    tabBarOptions: {
      activeTintColor: THEME.TEXT_COLOR,
      inactiveTintColor: THEME.ICONS_COLOR,
      showLabel: false,
      style: {
        paddingTop: 20,
        height: Platform.OS === 'android' ? 100 : 60,
        backgroundColor: THEME.BG_COLOR,
      },
      labelStyle: {
        fontSize: 12,
        fontFamily: 'Abel-Regular',
      },
    },
  },
);

export const AppNavigation = createAppContainer(FooterNavigation);
