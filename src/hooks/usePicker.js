import {useState} from 'react';
import storage from '../services/StorageService';

const updateSetting = async (name, state) => {
  const settings = await storage.getSettings();
  settings[name] = state;
  storage.setSettings(settings);
};

export const usePicker = () => {
  const [intervalPicker, setIntervalPicker] = useState(false);
  const [endPicker, setEndPicker] = useState(false);
  const [startPicker, setStartPicker] = useState(false);
  const [langPicker, setLanguagePicker] = useState(false);

  const setPicker = async (field, state) => {
    if (field) {
      await updateSetting(field, state);
    }
    switch (field) {
      case 'start':
        setStartPicker(state);
        break;
      case 'end':
        setEndPicker(state);
        break;
      case 'interval':
        setIntervalPicker(state);
        break;
      case 'language':
        setLanguagePicker(state);
        break;
      default:
        window.console.warn('undeclared property passed to picker hook');
        break;
    }
  };

  return [
    {
      intervalPicker,
      endPicker,
      startPicker,
      langPicker,
    },
    setPicker,
  ];
};
