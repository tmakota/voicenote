import {useState} from 'react';
import {DEFAULT_SETTINGS} from '../constants';
import storage from '../services/StorageService';

export const useSettings = () => {
  const [start, setStart] = useState(DEFAULT_SETTINGS.start);
  const [end, setEnd] = useState(DEFAULT_SETTINGS.end);
  const [interval, setInterval] = useState(DEFAULT_SETTINGS.interval);
  const [language, setLanguage] = useState(DEFAULT_SETTINGS.language);

  const getSettings = async () => {
    const settings = await storage.getSettings();
    setProperty('start', settings.start);
    setProperty('end', settings.end);
    setProperty('interval', settings.interval);
    setProperty('language', settings.language);
  };

  const setProperty = (field, data) => {
    switch (field) {
      case 'start':
        setStart(data);
        break;
      case 'end':
        setEnd(data);
        break;
      case 'interval':
        setInterval(data);
        break;
      case 'language':
        setLanguage(data);
        break;
      default:
        getSettings();
        break;
    }
  };

  return [{start, end, interval, language}, setProperty];
};
