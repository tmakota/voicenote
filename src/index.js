/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';

import {TodoState} from './context/todo/TodoState';
import {AppNavigation} from './navigation/AppNavigation';
import SplashScreen from 'react-native-splash-screen';

const App: () => React$Node = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <TodoState>
      <AppNavigation />
    </TodoState>
  );
};

export default App;
