import React, {useReducer} from 'react';
import {Alert} from 'react-native';
import {TodoContext} from './todoContext';
import {todoReducer} from './todoReducer';
import {ADD_TODO, REMOVE_TODO, UPDATE_TODO, SET_TODOS} from '../types';

export const TodoState = ({children}) => {
  const initialState = {
    todos: [],
  };

  const [state, dispatch] = useReducer(todoReducer, initialState);

  const addTodo = title => dispatch({type: ADD_TODO, title});

  const setTodos = todos => dispatch({type: SET_TODOS, todos});

  const removeTodo = id => {
    const todo = state.todos.find(t => t.id === id);
    Alert.alert(
      'Delete todo',
      `Are you sure you want delete "${todo.title}"?`,
      [
        {
          text: 'No',
          style: 'cancel',
        },
        {
          text: 'Yes',
          style: 'destructive',
          onPress: () => {
            dispatch({type: REMOVE_TODO, id});
          },
        },
      ],
      {cancelable: false},
    );
  };

  const updateTodo = (id, title) => dispatch({type: UPDATE_TODO, id, title});

  return (
    <TodoContext.Provider
      value={{
        todos: state.todos,
        addTodo,
        removeTodo,
        updateTodo,
        setTodos,
      }}>
      {children}
    </TodoContext.Provider>
  );
};
