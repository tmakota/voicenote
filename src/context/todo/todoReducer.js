import {ADD_TODO, REMOVE_TODO, SET_TODOS, UPDATE_TODO} from '../types';
import storage from '../../services/StorageService';

const handlers = {
  [ADD_TODO]: (state, {title}) => {
    const list = {
      ...state,
      todos: state.todos,
    };
    list.todos.unshift({ title, id: Date.now().toString() });
    storage.setList(list.todos);
    return list;
  },
  [REMOVE_TODO]: (state, {id}) => {
    const list = {
      ...state,
      todos: state.todos.filter(todo => todo.id !== id),
    };
    storage.setList(list.todos);
    return list;
  },
  [SET_TODOS]: (state, { todos }) => ({
    ...state,
    todos,
  }),
  [UPDATE_TODO]: (state, {title, id}) => {
    const list = {
      ...state,
      todos: state.todos.map(todo => {
        if (todo.id === id) {
          todo.title = title;
        }
        return todo;
      }),
    };
    storage.setList(list.todos);
    return list
  },
  DEFAULT: state => state,
};

export const todoReducer = (state, action) => {
  const handler = handlers[action.type] || handlers.DEFAULT;
  return handler(state, action);
};
