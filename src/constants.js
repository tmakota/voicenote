export const DEFAULT_SETTINGS = {
  start: '8',
  end: '22',
  interval: '120',
  language: 'uk-UA',
  enable: true,
};

export const MAX_AUDIO_LENGTH = 4;
export const DEFAULT_TEXT = 'no text yet';

export const LOCALES = [
  { lang: 'Ukraine',  code: 'uk-UA'},
  { lang: 'Deutschland',  code: 'de-DE'},
  { lang: 'United Kingdom',  code: 'en-GB'},
  { lang: 'United States',  code: 'en-US'},
  { lang: 'France',  code: 'fr-FR'},
  { lang: 'Italia',  code: 'it-IT'},
  { lang: 'Russia',  code: 'ru-RU'},
  { lang: 'China',  code: 'wuu-CN'},
  { lang: 'Israel',  code:'he-IL'},
  { lang: 'Polska',  code: 'pl-PL'},
  { lang: 'Turkey',  code: 'tr-TR'},
  { lang: 'Espanya',  code: 'ca-ES'},
];
