import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {THEME} from './theme';

export const Todo = ({todo, onRemove, onOpen}) => {
  return (
    <View style={styles.todo}>
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => onOpen(todo)}
        onLongPress={onRemove.bind(null, todo.id)}>
        <Text>{todo.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  todo: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: THEME.BG_COLOR,
    borderRadius: 5,
    elevation: 10,
    marginBottom: 15,
    marginLeft: 10,
    shadowOffset: {width: -5, height: 5},
    shadowColor: THEME.BLACK,
    shadowOpacity: 0.2,
    shadowRadius: 6,
  },
});
