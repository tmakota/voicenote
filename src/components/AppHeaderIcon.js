import React from 'react'
import { HeaderButton } from 'react-navigation-header-buttons'
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import { THEME } from './theme'

export const AppHeaderIcon = props => (
  <HeaderButton
    {...props}
    iconSize={24}
    IconComponent={Ionicons}
    color={THEME.BG_COLOR}
  />
);
