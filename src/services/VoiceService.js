import Voice from 'react-native-voice';
import { Platform } from 'react-native';
import storage from './StorageService';

class VoiceService {
  startRecording = async (cb) => {
    let isOn = false;
    if (!Voice.isAvailable()) {
      console.warn('speech recognition is not available');
      return isOn;
    }

    Voice.onSpeechError = result => {
      console.warn(result, 'onSpeechError');
    };

    Voice.onSpeechPartialResults = result => {
      cb(result.value.pop().toString());
    };

    const { language } = await storage.getSettings();
    if (Platform.OS === 'android') {
      const services = await Voice.getSpeechRecognitionServices();
      console.log(services, 'services');
      Voice.start(language, {
        RECOGNIZER_ENGINE: 'GOOGLE',
        EXTRA_PARTIAL_RESULTS: true,
      });
      return true;
    } else {
      await Voice.start(language);
      return true;
    }
  };

  stopRecognizing = async (cb) => {
    try {
      await Voice.stop();
      cb();
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
  };
}

const voice = new VoiceService();

export default voice;
