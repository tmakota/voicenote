import AsyncStorage from '@react-native-community/async-storage';
import {DEFAULT_SETTINGS} from '../constants';

class StorageService {
  getList = async () => {
    const list = await AsyncStorage.getItem('notes');
    if (!list) {
      this.setList([]);
      return [];
    }
    return JSON.parse(list);
  };

  setList = data => {
    AsyncStorage.setItem('notes', JSON.stringify(data));
  };

  getSettings = async () => {
    const settings = await AsyncStorage.getItem('settings');
    if (!settings) {
      this.setSettings(DEFAULT_SETTINGS);
      return DEFAULT_SETTINGS;
    }
    return JSON.parse(settings);
  };

  setSettings = data => {
    AsyncStorage.setItem('settings', JSON.stringify(data));
  };

  resetApp = () => {
    AsyncStorage.clear();
    return {};
  };
}

const storage = new StorageService();

export default storage;
