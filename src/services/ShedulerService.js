import BackgroundFetch from 'react-native-background-fetch';
import storage from './StorageService';
import {Platform, AppState} from 'react-native';
import notifications from './NotifService';

class ShedulerService {
  run = interval => {
    BackgroundFetch.configure(
      {
        minimumFetchInterval: Number(interval),
        // Android options
        stopOnTerminate: false,
        startOnBoot: true,
        forceReload: true,
        requiredNetworkType: BackgroundFetch.NETWORK_TYPE_NONE,
        requiresCharging: false,
        requiresDeviceIdle: false,
        requiresBatteryNotLow: false,
        requiresStorageNotLow: false,
      },
      async () => {
        if (AppState.currentState === 'active') {
          const settings = await storage.getSettings();
          return this.run(settings.interval);
        }
        BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
        this.sendNotification();
      },
      error => {
        console.error(
          `[js] RNBackgroundFetch failed to start ${JSON.stringify(error)}`,
        );
      },
    );
  };

  sendNotification = async () => {
    const list = await storage.getList();
    const settings = await storage.getSettings();
    const hour = new Date().getHours();
    const shouldNotificationSend =
      list.length > 0 && hour > settings.start && hour < settings.end;
    if (shouldNotificationSend) {
      // TODO remove when IOS certificate will be available
      if (Platform.OS === 'android') {
        list.forEach(item => {
          notifications.localNotif(item.title);
        });
      }
    }
    // run again task for check list and notification with interval
    this.run(settings.interval);
  };

  setList = async cb => {
    const list = await storage.getList();
    const {interval} = await storage.getSettings();
    this.run(Number(interval));
    cb(list);
  };
}

const sheduler = new ShedulerService();
export default sheduler;
