import {Dimensions, StyleSheet} from 'react-native';
import {THEME} from './components/theme';

export default StyleSheet.create({
  navbar: {
    backgroundColor: THEME.BG_COLOR,
  },
  body: {
    backgroundColor: THEME.BG_COLOR,
  },
  sectionContainer: {
    flex: 1,
    backgroundColor: THEME.BG_COLOR,
    padding: 20,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: THEME.ICONS_COLOR,
  },
  headerMain: {
    flex: 1,
    flexDirection: 'column-reverse',
  },
  header: {
    fontSize: 30,
    fontWeight: '600',
    color: THEME.ICONS_COLOR,
    paddingLeft: 40,
  },
  warningContainer: {
    padding: 20,
    flex: 1,
    justifyContent: 'center',
  },
  warning: {
    fontSize: 20,
    fontWeight: '600',
    color: THEME.WARNING,
    textAlign: 'center',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: THEME.BLACK,
    textAlign: 'center',
  },
  capture: {
    color: THEME.BLACK,
  },
  button: {
    borderWidth: 1,
    borderColor: THEME.BLACK,
    margin: 5,
    marginBottom: 15,
    padding: 5,
    width: '70%',
    backgroundColor: THEME.WHITE_GRAY,
    borderRadius: 5,
  },
  icons: {
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  icon: {
    width: 120,
    height: 120,
    padding: 10,
    borderRadius: 60,
    borderColor: THEME.MAIN_COLOR,
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  settingsRow: {
    flex: 1,
    borderBottomColor: THEME.GREY_COLOR,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  settingsRowColumn: {
    flex: 1,
    padding: 5,
  },
  recorder: {
    width: 120,
    height: 120,
    borderRadius: 60,
    marginTop: -20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pickerStyles: {
    height: 50,
    width: Dimensions.get('window').width - 100,
  },
});
