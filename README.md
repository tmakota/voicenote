## Voice note app

### libraries

[react-native-voice](https://github.com/react-native-community/react-native-voice)

[react-native-push-notification](https://github.com/zo0r/react-native-push-notification)

[reactnavigation](https://reactnavigation.org/en/)

[react-navigation-header-buttons](https://github.com/vonovak/react-navigation-header-buttons)

[react-native-vector-icons](https://github.com/oblador/react-native-vector-icons)

[async-storage](https://github.com/react-native-community/async-storage)

[react-native-background-fetch](https://github.com/transistorsoft/react-native-background-fetch)

[react-native-splash-screen](https://github.com/crazycodeboy/react-native-splash-screen)
 
### run project

npx react-native run-android 

npx react-native run-ios 

### install via command line

react-native run-android --variant=release 

#### tips

open debug menu on Android device

```
adb shell input keyevent 82
```

run task from background

```
adb shell cmd jobscheduler run -f com.voicenote 999
```
